package Main;

import Annotations.Entity;
import Annotations.Table;
import Dao.InvoiceDao;
import Dao.UserDao;
import domain.Invoice;
import domain.User;



public class Main {

	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		
		Invoice invoice = new Invoice("Prvi invoice", 89);
		invoice.setId(89);
		invoice.setName("Prvi invoice");
		Entity en = invoice.getClass().getAnnotation(Entity.class);
		//System.out.println("Anotacija je: "+en);
		Table tb = invoice.getClass().getAnnotation(Table.class);
		//System.out.println("Table anotacija: "+tb);
		//System.out.println("Atribut table anotacije:"+ tb.name());
		InvoiceDao in = new InvoiceDao();
		  
		
		User u = new User(4, "Preza", "neki mejl");
		UserDao userDao = new UserDao();
		invoice.setUser(u);
		in.save(invoice);
		in.update(invoice,"id","5");
		in.findAll(invoice);
		in.findByGeneric("id", "7", invoice);
		in.createTable(invoice);
		
		
		
		
		userDao.save(u);
	}

}
