package Dao;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Annotations.Column;
import Annotations.ManyToOne;
import Annotations.Table;
import domain.HelpClass;
import domain.Invoice;
import domain.User;

public class UserDao {
	
public Invoice save(User user) throws ClassNotFoundException {
		
		String query = "INSERT INTO ";
		Class cl = Class.forName("domain.User");
		
		Table[] tableNameAnnotations = user.getClass().getAnnotationsByType(Table.class);
		String tableName = tableNameAnnotations[0].name();
		
		HashMap<String, HelpClass> hashMap = new HashMap<>();	
		
			try {
				Object obj = cl.newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
						
			Field[] fields = cl.getDeclaredFields();
			
			ArrayList<Field> fieldsForMtoO = new ArrayList<>();
			
			for (Field field : fields) {
				if(field.getAnnotation(ManyToOne.class) != null) {
					fieldsForMtoO.add(field);
				}
			}
			
			for (Field field : fieldsForMtoO) {
				System.out.println("O2M--"+field);
			}
			
			hashMap = findHash(fields, cl, user); 
			System.out.println(makeQuery(hashMap, tableName, query));
		
		return null;
	}

	public String regexString(String name) {
		
		String[] nameParts = name.split("\\.");
		String realName = "";
		
		if(nameParts.length > 1) {
			realName = nameParts[2];
		}else {
			realName = name;
		}
		return realName;
	}

	public HashMap<String,HelpClass> findHash(Field[] fields,Class cl,User user){
		
		HashMap<String,HelpClass> hashMap = new HashMap<>();
		
		for (Field field : fields) {
			String[] splits = field.toString().split(" ");
			String type = splits[1];
			String name = splits[2];
			
			Field field2;
			try {
				field2 = cl.getDeclaredField(regexString(name));
				field2.setAccessible(true);
				
				if(field2.isAnnotationPresent(Column.class)) {
					HelpClass helper = new HelpClass(regexString(type), field2.get(user)+"");
					hashMap.put(field2.getAnnotation(Column.class).name(), helper);
				}	
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}					
		}
		return hashMap;
	}

public String makeQuery(HashMap<String,HelpClass> hashMap,String tableName,String query) {
	
	StringBuilder sb = new StringBuilder(query);
	sb.append(tableName);
	sb.append(" (");
	for(Map.Entry<String, HelpClass> entry : hashMap.entrySet()) {
	    String key = entry.getKey();
	    HelpClass value = entry.getValue();
	    sb.append(key+",");
	}
	sb.deleteCharAt(sb.length()-1);
	sb.append(")");
	sb.append(" ");
	sb.append("values( ");
	for(Map.Entry<String, HelpClass> entry : hashMap.entrySet()) {
	    String key = entry.getKey();
	    HelpClass value = entry.getValue();
	    
	    if(value.getType().equals("String")) {
	    	sb.append("'"+value.getValue()+"',");
	    }else {
	    	sb.append(value.getValue()+",");	
	    }
	}
	sb.deleteCharAt(sb.length()-1);
	sb.append(");");
	return sb.toString();
}

}