package Dao;

public aspect DaoAspect {
	
	pointcut pozoviDrugiDao() :call(public String UserDao.save());

	after() : pozoviDrugiDao() {
		System.out.println("Aspect je odradio akciju!");
	}
}
