package Dao;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Annotations.Column;
import Annotations.ManyToOne;
import Annotations.Table;
import domain.HelpClass;
import domain.Invoice;
import domain.User;

public class InvoiceDao {

public Invoice save(Invoice invoice) throws ClassNotFoundException {
		
		String query = "";
		Class cl = Class.forName("domain.Invoice");
		
		StringBuilder sb = new StringBuilder(query);
		
		Table[] tableNameAnnotations = invoice.getClass().getAnnotationsByType(Table.class);
		String tableName = tableNameAnnotations[0].name();
		
		HashMap<String, HelpClass> hashMap = new HashMap<>();	
		
			try {
				Object obj = cl.newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
						
			Field[] fields = cl.getDeclaredFields();
			
//			//Provera za MENYTOONE  private domain.User domain.Invoice.user
			ArrayList<Field> fieldsForMtoO = new ArrayList<>();
			
			for (Field field : fields) {
				if(field.getAnnotation(ManyToOne.class) != null) {
					fieldsForMtoO.add(field);
				}
			}
			
			if(!fieldsForMtoO.isEmpty()) {
				for (Field field : fieldsForMtoO) {
					
					String[] splits = field.toString().split(" ");
					String type = splits[1];
					String name = splits[2];
					
					String classN = "";
					String[] regexClassName = type.split("\\.");
					classN = regexClassName[1];
					System.out.println("CLASS NAME::::: "+classN);
					
					Class cls = Class.forName("domain."+classN);
					
					System.out.println("CLS+ + "+cls);
					
					Field[] fls =  invoice.getClass().getDeclaredFields();
					
					for (Field field2 : fls) {
						
						String dm = "class "+type;
						String fieldType = field2.getType().toString();
						if(field2.getType().toString().equals(dm)) {
							Object objc = null;
							try {
								field2.setAccessible(true);
								objc = field2.get(invoice);
								
								System.out.println(objc.toString());
							} catch (IllegalArgumentException e) {
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							}
							Table[] ans = objc.getClass().getAnnotationsByType(Table.class);
							Field[] asFields = objc.getClass().getDeclaredFields();
							HashMap<String, HelpClass> hash = findHashObj(asFields, cls, objc);
							/**
							 * posle ovog make query ide execute pa id ide u to polje sa kojim 
							 * se gore poklopilo
							 */
							System.out.println("hash leght"+hash.size());
							String queryFirst = makeInsertQuery(hash,ans[0].name());
							sb.append(queryFirst);
						}
					}					
				}
			}
			hashMap = findHashObj(fields, cl, invoice); 
			
			String query2 = makeInsertQuery(hashMap, tableName);
			sb.append("\n");
			sb.append(query2);
		    System.out.println(sb.toString());
		return null;
	}
	
	public Invoice update(Invoice invoice,String conditionName,String conditionValue) {
				
		Class cl = null;
		String qury ="";
		StringBuilder sb = new StringBuilder(qury);
        try {
			cl = Class.forName("domain.Invoice");
			Object obj = cl.newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		Table[] tableNameAnnotations = invoice.getClass().getAnnotationsByType(Table.class);
		String tableName = tableNameAnnotations[0].name();
		
		HashMap<String, HelpClass> hashMap = new HashMap<>();	
					
		Field[] fields = cl.getDeclaredFields();
		hashMap = findHashObj(fields, cl, invoice);
		
		//+++++++
		//Provera za MENYTOONE  private domain.User domain.Invoice.user
		ArrayList<Field> fieldsForMtoO = new ArrayList<>();
		
		for (Field field : fields) {
			if(field.getAnnotation(ManyToOne.class) != null) {
				fieldsForMtoO.add(field);
			}
		}
		
		if(!fieldsForMtoO.isEmpty()) {
			for (Field field : fieldsForMtoO) {
				
				String[] splits = field.toString().split(" ");
				String type = splits[1];
				String name = splits[2];
				
				String classN = "";
				String[] regexClassName = type.split("\\.");
				classN = regexClassName[1];
				
				Class cls = null;
				try {
					cls = Class.forName("domain."+classN);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
								
				Field[] fls =  invoice.getClass().getDeclaredFields();
				
				for (Field field2 : fls) {
					
					String dm = "class "+type;
					String fieldType = field2.getType().toString();
					if(field2.getType().toString().equals(dm)) {
						Object objc = null;
						try {
							field2.setAccessible(true);
							objc = field2.get(invoice);
							
							System.out.println(objc.toString());
						} catch (IllegalArgumentException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						}
						Table[] ans = objc.getClass().getAnnotationsByType(Table.class);
						Field[] asFields = objc.getClass().getDeclaredFields();
						HashMap<String, HelpClass> hash = findHashObj(asFields, cls, objc);
						/**
						 * posle ovog make query ide execute pa id ide u to polje sa kojim 
						 * se gore poklopilo
						 */
						System.out.println("hash leght"+hash.size());
						String queryFirst = makeUpdateQuery(hash, tableName, "", "");
						sb.append(queryFirst);
					}
				}					
			}
		}
		//+++++++
		
		sb.append("\n");
		sb.append(makeUpdateQuery(hashMap, tableName, conditionName, conditionValue));
		System.out.println("kocani____");
		System.out.println(sb.toString());
		System.out.println("kraj_______");
		return null;
	}
	
	public ArrayList<Invoice> findAll(Invoice invoice){
		
		ArrayList<Invoice> invoiceList = new ArrayList<>();
		
		
		String query = "SELECT * from ";
		
		Class cl = null;
        try {
			cl = Class.forName("domain.Invoice");
			Object obj = cl.newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		Table[] tableNameAnnotations = invoice.getClass().getAnnotationsByType(Table.class);
		String tableName = tableNameAnnotations[0].name();
		
		query = query+tableName+";";
		System.out.println(query);
		
		return invoiceList;
	}
	
	public ArrayList<Invoice> findBy(String condition,String value,Invoice invoice){
		
		String query = "SELECT * FROM ";
		User u = new User(2, "ooo", "mail");
		findByGeneric("id", u.getId()+"", u);
		Class cl = null;
        try {
			cl = Class.forName("domain.Invoice");
			Object obj = cl.newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		Table[] tableNameAnnotations = invoice.getClass().getAnnotationsByType(Table.class);
		String tableName = tableNameAnnotations[0].name();
		
		query = query+tableName+" WHERE ";
		query =  query+""+condition+" = "+value+";";
		
		System.out.println(query);
		
		//invoice isti for za many
		ArrayList<Invoice> invoiceList = new ArrayList<>();
		return invoiceList;
		
	}
	
	public ArrayList<Invoice> findByGeneric(String condition,String value,Object object){
		
		String query = "SELECT * FROM ";
		
		Class cl = null;
        try {
			cl = Class.forName(object.getClass().getName());
			Object obj = cl.newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		Table[] tableNameAnnotations = object.getClass().getAnnotationsByType(Table.class);
		String tableName = tableNameAnnotations[0].name();
		
		query = query+tableName+" WHERE ";
		query =  query+""+condition+" = "+value+";";
		
		System.out.println("Ovaj dole");
		System.out.println(query);
		
		
		ArrayList<Invoice> invoiceList = new ArrayList<>();
		return invoiceList;
		
	}
	
	public String createTable(Invoice invoice) {
		
		String query = "CREATE TABLE ";
		
		Class cl = null;
        try {
			cl = Class.forName("domain.Invoice");
			Object obj = cl.newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		Table[] tableNameAnnotations = invoice.getClass().getAnnotationsByType(Table.class);
		String tableName = tableNameAnnotations[0].name();
		
		query = query+tableName;
		
		HashMap<String, HelpClass> hashMap = new HashMap<>();	
		
		Field[] fields = cl.getDeclaredFields();
		hashMap = findHashObj(fields, cl, invoice); 

		StringBuilder sb = new StringBuilder(query);
		sb.append(" (");
		for(Map.Entry<String, HelpClass> entry : hashMap.entrySet()) {
		    String key = entry.getKey();
		    HelpClass value = entry.getValue();
		    if(value.getType().equals("String")) {
		    	sb.append(key+"  varchar(225),");
		    }else {
		    	sb.append(key+"  "+value.getType()+",");	
		    }
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append(");");
		
		System.out.println(sb.toString());
		return sb.toString();
	}
	
	
	/**
	 * HELP FUNCTIONS
	 */
	
	public String regexString(String name) {
		
		String[] nameParts = name.split("\\.");
		String realName = "";
		
		if(nameParts.length > 1) {
			realName = nameParts[2];
		}else {
			realName = name;
		}
		return realName;
	}
	
	public HashMap<String,HelpClass> findHashObj(Field[] fields,Class cl,Object invoice){
		
		HashMap<String,HelpClass> hashMap = new HashMap<>();
		
		for (Field field : fields) {
			String[] splits = field.toString().split(" ");
			String type = splits[1];
			String name = splits[2];
			
			Field field2;
			try {
				field2 = cl.getDeclaredField(regexString(name));
				field2.setAccessible(true);
				
				if(field2.isAnnotationPresent(Column.class)) {
					HelpClass helper = new HelpClass(regexString(type), field2.get(invoice)+"");
					hashMap.put(field2.getAnnotation(Column.class).name(), helper);
				}	
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}					
		}
		return hashMap;
	}
	
	public String makeInsertQuery(HashMap<String,HelpClass> hashMap,String tableName) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append(tableName);
		sb.append(" (");
		for(Map.Entry<String, HelpClass> entry : hashMap.entrySet()) {
		    String key = entry.getKey();
		    HelpClass value = entry.getValue();
		    sb.append(key+",");
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append(")");
		sb.append(" ");
		sb.append("values( ");
		for(Map.Entry<String, HelpClass> entry : hashMap.entrySet()) {
		    String key = entry.getKey();
		    HelpClass value = entry.getValue();
		    
		    if(value.getType().equals("String")) {
		    	sb.append("'"+value.getValue()+"',");
		    }else {
		    	sb.append(value.getValue()+",");	
		    }
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append(");");
		return sb.toString();
	}
	
	public String makeUpdateQuery(HashMap<String, HelpClass> hashMap,String tableName,String conditionName,String conditionValue) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append(tableName);		
		sb.append(" SET ");
		sb.append(" (");
		for(Map.Entry<String, HelpClass> entry : hashMap.entrySet()) {
		    String key = entry.getKey();
		    HelpClass value = entry.getValue();
		    
		    if(value.getType().equals("String")) {
		    	sb.append(key+" = '"+value.getValue()+"',");
		    }else {
		    	sb.append(key+" = "+value.getValue()+",");	
		    }
		    
		    
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append(") ");

		if(conditionName.equals("") || conditionValue.equals("")) {
			sb.append("WHERE id > 0;");
		}else {
			sb.append("WHERE "+conditionName+" = "+conditionValue+";");
		}
		return sb.toString();
	}
}
