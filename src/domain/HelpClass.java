package domain;

public class HelpClass {
	
	private String type;
	private String value;
	
	public HelpClass() {
		
	}
	
	public HelpClass(String type,String value) {
		this.value = value;
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

}

