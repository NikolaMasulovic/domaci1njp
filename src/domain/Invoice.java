package domain;

import Annotations.Column;
import Annotations.Entity;
import Annotations.ID;
import Annotations.JoinColumn;
import Annotations.ManyToOne;
import Annotations.Table;
import Annotations.Transient;

@Entity
@Table(name="Invoice")
public class Invoice {
	
	@ID(autoIncrement=true)
	@Column(name="id")
	private int id;
	
	@Column(name="nameInovice")
	private String name;
	
	@Transient
	private String date;
	
	@ManyToOne
	@JoinColumn(name="user_id", nullable=false)
	private User user;
	
	
	public Invoice(String name,int id) {
		this.name = name;
		this.id = id;
	}
	
	public Invoice() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Invoice [name=" + name + ", id=" + id + "]";
	}
	
	
}
