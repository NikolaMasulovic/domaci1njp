package domain;

import java.util.ArrayList;
import java.util.List;

import Annotations.Column;
import Annotations.Entity;
import Annotations.ID;
import Annotations.OneToMeny;
import Annotations.Table;

@Entity
@Table(name="User")
public class User {
	
	@ID(autoIncrement=true)
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="user_email")
	private String email;
	
	@OneToMeny(mappedBy="User")
	private List<Invoice> invoices = new ArrayList<>();
	
	public User() {
		
	}
	
	public User(int id,String name,String email) {
		this.id = id;
		this.name = name;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<Invoice> invoices) {
		this.invoices = invoices;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", email=" + email + "]";
	}
}
